import functools

import pika
from pika.exchange_type import ExchangeType

from running_max import RunningMax, RandomNumber


class Consumer:

    def __init__(self, amqp_url: str, topic: str, running_max: RunningMax):
        self.url = amqp_url
        self.topic = topic

        parameters = pika.URLParameters(amqp_url)
        self.connection = pika.BlockingConnection(parameters)

        channel = self.connection.channel()
        channel.exchange_declare(
            exchange=topic,
            exchange_type=ExchangeType.topic,
            passive=False,
            durable=True,
            auto_delete=False)

        self.channel = channel
        on_message_callback = functools.partial(
            self.on_message, running_max=running_max)
        self.channel.basic_consume(self.topic, on_message_callback)

    def run(self):
        """
        Handles consumer lifecycle
        """
        try:
            self.channel.start_consuming()
        except KeyboardInterrupt:
            self.channel.stop_consuming()

        self.connection.close()

        self.channel.start_consuming()

    @staticmethod
    def on_message(connection_channel, method_frame, header_frame, body, running_max: RunningMax):
        """
        Message received callback from rabbitmq consumer
        """
        decoded_body = eval(body.decode())
        rand = RandomNumber(
            sequence_number=decoded_body['sequence_number'],
            rand=decoded_body['rand']
        )
        running_max.insert(rand)
        connection_channel.basic_ack(delivery_tag=method_frame.delivery_tag)
