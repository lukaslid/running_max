from queue import Queue

import pika


class Publisher:
    def __init__(self, amqp_url: str, topic: str, queue: Queue):
        self.url = amqp_url
        self.topic = topic
        self.queue = queue

        parameters = pika.URLParameters(amqp_url)
        connection = pika.BlockingConnection(parameters)

        channel = connection.channel()
        self.channel = channel
        self.connection = connection

    def publish(self, message):
        """
        publishes one message to solution topic
        """
        self.channel.basic_publish(
            '', self.topic, message,
            pika.BasicProperties(content_type='text/plain'))

    def run(self):
        """
        loops over queue to publish results
        """
        while True:
            data = self.queue.get()
            self.publish(data)
