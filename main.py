import queue
from threading import Thread

from constants import BROKER_URL, DATA_TOPIC, SOLUTION_TOPIC
from message_broker.consumer import Consumer
from message_broker.publisher import Publisher
from running_max import RunningMax

if __name__ == '__main__':
    publisher_queue = queue.Queue(maxsize=50)
    running_max = RunningMax(q=publisher_queue)

    consumer = Consumer(BROKER_URL, DATA_TOPIC, running_max)
    publisher = Publisher(BROKER_URL, SOLUTION_TOPIC, publisher_queue)

    p = Thread(target=consumer.run)
    p.start()

    p2 = Thread(target=publisher.run)
    p2.start()
