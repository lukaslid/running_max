import json
from dataclasses import dataclass
from queue import Queue

import numpy as np
from numba import njit


@dataclass
class RandomNumber(object):
    """
    Input class for inputs form rabbitmq random number topic
    """
    sequence_number: int
    rand: int


@dataclass
class RunningMax(object):
    """
    Stores data and calculates running mean
    """

    q: Queue
    max: int = None
    np_rands: np.ndarray = np.full(100, -1, dtype=np.int64)
    np_seq: np.ndarray = np.full(100, -1, dtype=np.int64)
    current_id: int = 0

    def is_new_max(self, r):
        if self.max is None:
            return True

        if r.rand > self.max:
            return True

        return False

    def insert(self, r: RandomNumber):
        if self.current_id == 100:  # reset id for replacing
            self.current_id = 0

        if self.is_new_max(r):
            self.max = r.rand

        if self.np_seq[self.current_id] != -1:
            self.replace(r)
        else:
            self._insert(r)

        self.current_id += 1
        self.push_to_queue(r, self.max)

    def _insert(self, r: RandomNumber):
        self.np_rands[self.current_id] = r.rand
        self.np_seq[self.current_id] = r.sequence_number

    def push_to_queue(self, r, maximum):
        data_dict = {
            'rand': r.rand,
            'sequence_number': r.sequence_number,
            'running_max': int(maximum)
        }
        self.q.put(json.dumps(data_dict).encode('utf-8'))

    def replace(self, r: RandomNumber):
        rand = self.np_rands[self.current_id]

        self._insert(r)

        if rand == self.max:
            self.max = get_maximum(self.np_rands)


@njit
def get_maximum(numbers):
    return numbers.max()
